import time

import numpy as np
import pandas as pd
from sklearn import preprocessing
from sklearn.ensemble import RandomForestRegressor
from sklearn.metrics import mean_absolute_error
from sklearn.metrics import mean_squared_error
from sklearn.metrics import r2_score
from sklearn.model_selection import train_test_split
from sklearn.neural_network import MLPRegressor
from sklearn.svm import SVR
from xgboost import XGBRegressor

data = pd.read_csv("../data/TCM_TK_TH_season.csv")
data = data.to_numpy()

y = data[:, -1].reshape(-1, 1)
X = data[:, :-1]

mlp_result = []
rf_result = []
svm_result = []
xgb_result = []

for i in range(1, 51):
    X_train, X_test, y_train, y_test = train_test_split(X, y, test_size=.25, random_state=i * 13)

    scaler = preprocessing.StandardScaler()

    X_train = scaler.fit_transform(X_train)
    X_test = scaler.transform(X_test)
    y_train = scaler.fit_transform(y_train)
    y_test = scaler.transform(y_test)

    mlp = MLPRegressor(hidden_layer_sizes=(192, 128, 96), max_iter=1000, learning_rate_init=0.01, tol=1e-6,
                       batch_size=192)
    start_time = time.time()
    mlp.fit(X_train, y_train)
    training_time = time.time() - start_time
    mlp_pred = mlp.predict(X_test)
    r2 = r2_score(y_test, mlp_pred)
    mae = mean_absolute_error(y_test, mlp_pred)
    rmse = np.sqrt(mean_squared_error(y_test, mlp_pred))
    print("MLP regression score: {:.4f}".format(r2))
    print("MLP regression MAE: {:.4f}".format(mae))
    print("MLP regression RMSE: {:.4f}".format(rmse))
    print("Time: {}".format(training_time))
    mlp_result.append([r2, mae, rmse, training_time])

    rf_regression = RandomForestRegressor(n_estimators=200, random_state=42)
    start_time = time.time()
    rf_regression.fit(X_train, y_train)
    training_time = time.time() - start_time
    rf_pred = rf_regression.predict(X_test)
    r2 = r2_score(y_test, rf_pred)
    mae = mean_absolute_error(y_test, rf_pred)
    rmse = np.sqrt(mean_squared_error(y_test, rf_pred))
    print("RF regression score: {:.4f}".format(r2))
    print("RF regression MAE: {:.4f}".format(mae))
    print("RF regression RMSE: {:.4f}".format(rmse))
    print("Time: {}".format(training_time))
    rf_result.append([r2, mae, rmse, training_time])

    svr = SVR(gamma='auto', C=100, epsilon=0.0001)
    start_time = time.time()
    svr.fit(X_train, y_train)
    training_time = time.time() - start_time
    svr_pred = svr.predict(X_test)
    r2 = r2_score(y_test, svr_pred)
    mae = mean_absolute_error(y_test, svr_pred)
    rmse = np.sqrt(mean_squared_error(y_test, svr_pred))
    print("SVM regression score: {:.4f}".format(r2))
    print("SVM regression MAE: {:.4f}".format(mae))
    print("SVM regression RMSE: {:.4f}".format(rmse))
    print("Time: {}".format(training_time))
    svm_result.append([r2, mae, rmse, training_time])

    xgb_regession = XGBRegressor(n_estimators=200, objective='reg:squarederror')
    start_time = time.time()
    xgb_regession.fit(X_train, y_train)
    training_time = time.time() - start_time
    xgb_pred = xgb_regession.predict(X_test)
    r2 = r2_score(y_test, xgb_pred)
    mae = mean_absolute_error(y_test, xgb_pred)
    rmse = np.sqrt(mean_squared_error(y_test, xgb_pred))
    print("XGB regression score: {:.4f}".format(r2))
    print("XGB regression MAE: {:.4f}".format(mae))
    print("XGB regression RMSE: {:.4f}".format(rmse))
    print("Time: {}".format(training_time))
    xgb_result.append([r2, mae, rmse, training_time])

    print(i)

mlp_result = np.asarray(mlp_result)
rf_result = np.asarray(rf_result)
svm_result = np.asarray(svm_result)
xgb_result = np.asarray(xgb_result)

np.savetxt("../result/result_mlp.csv", mlp_result, delimiter=",")
np.savetxt("../result/result_rf.csv", rf_result, delimiter=",")
np.savetxt("../result/result_svm.csv", svm_result, delimiter=",")
np.savetxt("../result/result_xgb.csv", xgb_result, delimiter=",")

mlp_result_avg = np.average(np.asarray(mlp_result), axis=0)
rf_result_avg = np.average(np.asarray(rf_result), axis=0)
svm_result_avg = np.average(np.asarray(svm_result), axis=0)
xgb_result_avg = np.average(np.asarray(xgb_result), axis=0)

print("MLP: {}".format(mlp_result_avg))
print("RF: {}".format(rf_result_avg))
print("SVM: {}".format(svm_result_avg))
print("XGB: {}".format(xgb_result_avg))
