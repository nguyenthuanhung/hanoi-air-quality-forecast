import pandas as pd
import seaborn as sns
import matplotlib.pyplot as plt


# filename = "HANOI_DATA"
# filename = "VNPT_TH"
# filename = "TRINH_KHA"
# filename = "THINKLABS"
# df = pd.read_csv(filename + ".csv")
# df = df.drop(['time', 'node'], axis=1)
# df = df.drop(['time', 'SO2', 'CO', 'NH3', 'O3', 'AQI', 'PM01', 'TVOC'], axis=1)
# df = df.drop(df[df.TEMP > 45].index)
# df = df.drop(df[df.HUM > 100].index)
# df = df.drop(df[df.LIGHT > 800].index)
# df = df.dropna()

df = pd.read_csv("../data/iot-aqm.continuousdata.csv")
df = df.drop(["_id", "data.0.status", "data.0.unit", "data.1.status", "data.1.unit", "data.2.status", "data.2.unit",
              "data.3.status", "data.3.unit", "data.4.status", "data.4.unit", "data.5.status",
              "data.5.unit", "data.6.status", "data.6.unit", "node", "oganizationCode"], axis=1)

even_df = df.iloc[::2]
even_df = even_df.reset_index()
even_df = even_df.drop(["index"], axis=1)

odd_df = df.iloc[1::2]
odd_df = odd_df.reset_index()
odd_df = odd_df.drop(["index", "data.6.index", "data.6.value", "time"], axis=1)

df = pd.concat([odd_df, even_df], axis=1, sort=False)
print(df.columns)
columns = ["col1", "SO2", "col2", "CO", "col3", "NH3", "col4", "O3", "col5", "PM25", "col6", "PM10", "col7", "AQI",
           "col8", "CO2", "col9", "PM01", "col10", "TVOC", "col11", "TEMP", "col12", "HUM", "col13", "LIGHT", "time"]
df.columns = columns

df = df.drop(
    ["col1", "col2", "col3", "col4", "col5", "col6", "col7", "col8", "col9", "col10", "col11", "col12", "col13"],
    axis=1)
df = df.drop(
    ['SO2', 'CO', 'NH3', 'O3', 'AQI', 'PM01', 'TVOC'],
    axis=1)
df = df.dropna()
df = df.drop(df[df.TEMP > 45].index)
df = df.drop(df[df.HUM > 100].index)
df = df.drop(df[df.LIGHT > 800].index)
df = df.drop(df[df.PM25 > 500].index)
df = df.drop(df[df.PM10 > 500].index)


fig, axs = plt.subplots(ncols=3, nrows=2, figsize=(12, 8))
sns.distplot(df["PM25"], ax=axs[0, 0]).set_title("PM25")
sns.distplot(df["PM10"], ax=axs[0, 1]).set_title("PM10")
sns.distplot(df["CO2"], ax=axs[0, 2], kde=False).set_title("CO2")
sns.distplot(df["TEMP"], ax=axs[1, 0]).set_title("TEMP")
sns.distplot(df["HUM"], ax=axs[1, 1]).set_title("HUM")
sns.distplot(df["LIGHT"], ax=axs[1, 2]).set_title("LIGHT")
# sns.distplot(df["CO"], ax=axs[3, 0], kde=False).set_title("CO")
plt.show()
