import datetime
import pandas as pd
import seaborn as sns
import numpy as np
import matplotlib.pyplot as plt

df = pd.read_csv("../data/iot-aqm.continuousdata.csv")
df = df.drop(["_id", "data.0.status", "data.0.unit", "data.1.status", "data.1.unit", "data.2.status", "data.2.unit",
              "data.3.status", "data.3.unit", "data.4.status", "data.4.unit", "data.5.status",
              "data.5.unit", "data.6.status", "data.6.unit",
              "node", "oganizationCode"], axis=1)

# df = df.drop(["_id", "data.0.status", "data.0.unit", "data.1.index", "data.1.status", "data.1.unit",
#               "data.1.value", "data.2.index", "data.2.status", "data.2.unit", "data.2.value",
#               "data.3.status", "data.3.unit", "data.4.status", "data.4.unit", "data.5.status",
#               "data.5.unit", "data.6.index", "data.6.status", "data.6.unit", "data.6.value",
#               "node", "oganizationCode"], axis=1)

even_df = df.iloc[::2]
even_df = even_df.reset_index()
# even_df = even_df.drop(["index", "data.0.index", "data.0.value", "data.3.index", "data.3.value"], axis=1)
even_df = even_df.drop(["index"], axis=1)

odd_df = df.iloc[1::2]
odd_df = odd_df.reset_index()
odd_df = odd_df.drop(["index", 'data.6.index', 'data.6.value', "time"], axis=1)
# odd_df = odd_df.drop(["index", 'data.6.index', 'data.6.value'], axis=1)

df = pd.concat([odd_df, even_df], axis=1, sort=False)
# print(df.columns)
# columns = ["col1", "CO2", "col2", "TEMP", "col3", "HUM", "col4", "LIGHT", "col5", "PM25", "col6", "PM10", "time"]
columns = ["col1", "CO2", "col2", "PM01", "col3", "TVOC", "col4", "TEMP", "col5", "HUM", "col6", "LIGHT", "col7",
"SO2", "col8", "CO", "col9", "NH3", "col10", "O3", "col11", "PM25", "col12", "PM10", "col13", "AQI", "time"]
df.columns = columns

# df['time'] = pd.to_datetime(df['time'])
# df['time'] += datetime.timedelta(hours=7)
#
# times = pd.DatetimeIndex(df.time)
# df = df.groupby([times.year, times.month, times.day, times.dayofweek, times.hour]).mean()
# df.index.names = ["Year", "Month", "Day", "Weekend", "Hour"]

# df = df.drop(["col1", "col2", "col3", "col4", "col5", "col6"], axis=1)
df = df.drop(["col1", "col2", "col3", "col4", "col5", "col6", "col7", "col8", "col9", "col10", "col11", "col12", "col13"], axis=1)
# df = df.drop(["col1", "col2", "col3", "col4", "col5", "col6", "col7", "col8", "col9", "col10", "col11", "col12", "col13",
#               "PM01", "TVOC", "SO2", "CO", "NH3", "AQI", "O3"], axis=1)

# print(df.columns)
df = df.dropna()
df = df.drop(df[df.TEMP > 50].index)
df = df.drop(df[df.HUM > 100].index)
df = df.drop(df[df.CO2 > 5000].index)
df = df.drop(df[df.CO2 < 400].index)
df = df.drop(df[df.LIGHT > 800].index)
df = df.drop(df[df.PM25 > 200].index)
df = df.drop(df[df.PM10 > 200].index)

df = df.drop(df[df.NH3 < 0].index)
df = df.drop(df[df.PM01 < 0].index)

# fig, axs = plt.subplots(ncols=3, nrows=2, figsize=(12, 8))
# font_size = 15
# sns.scatterplot(df["PM25"], df["PM01"], ax=axs[0][0])
# sns.scatterplot(df["PM25"], df["TVOC"], ax=axs[0][1])
# sns.scatterplot(df["PM25"], df["SO2"], ax=axs[0][2])
# sns.scatterplot(df["PM25"], df["NH3"], ax=axs[1][0])
# sns.scatterplot(df["PM25"], df["O3"], ax=axs[1][1])
# sns.scatterplot(df["PM25"], df["PM10"], ax=axs[1][2])
#
# axs[0][0].set_ylabel("PM$_{0.1}$", fontsize=font_size)
# axs[0][1].set_ylabel("TVOC", fontsize=font_size)
# axs[0][2].set_ylabel("SO$_2$", fontsize=font_size)
# axs[1][0].set_ylabel("NH$_3$", fontsize=font_size)
# axs[1][1].set_ylabel("O$_3$", fontsize=font_size)
# axs[1][2].set_ylabel("PM$_10$", fontsize=font_size)
#
# for ax in axs:
#     for a in ax:
#         a.set_xlabel("PM$_{2.5}$", fontsize=font_size)
# plt.show()

# fig, axs = plt.subplots(ncols=3, nrows=1, figsize=(12, 4))
# sns.distplot(df["TEMP"], ax=axs[0]).set_title("Temperature")
# sns.distplot(df["HUM"], ax=axs[1]).set_title("Humidity")
# sns.distplot(df["LIGHT"], ax=axs[2]).set_title("Light")
# for ax in axs:
#     ax.set_xlabel("")
# plt.show()

# fig, axs = plt.subplots(ncols=3, nrows=4, figsize=(12, 16))
# font_size = 15
# sns.distplot(df["TEMP"], ax=axs[0][0]).set_title("Nhiệt độ", fontsize=font_size)
# sns.distplot(df["HUM"], ax=axs[0][1]).set_title("Độ ẩm", fontsize=font_size)
# sns.distplot(df["LIGHT"], ax=axs[0][2]).set_title("Ánh sáng", fontsize=font_size)
# sns.distplot(df["CO2"], ax=axs[1][0]).set_title("CO$_2$", fontsize=font_size)
# sns.distplot(df["PM01"], ax=axs[1][1]).set_title("PM$_{0.1}$", fontsize=font_size)
# sns.distplot(df["TVOC"], ax=axs[1][2]).set_title("TVOC", fontsize=font_size)
# sns.distplot(df["SO2"], ax=axs[2][0]).set_title("SO$_2$", fontsize=font_size)
# sns.distplot(df["CO"], ax=axs[2][1]).set_title("CO", fontsize=font_size)
# sns.distplot(df["NH3"], ax=axs[2][2]).set_title("NH$_3$", fontsize=font_size)
# sns.distplot(df["O3"], ax=axs[3][0]).set_title("O$_3$", fontsize=font_size)
# sns.distplot(df["PM25"], ax=axs[3][1]).set_title("PM$_{2.5}$", fontsize=font_size)
# sns.distplot(df["PM10"], ax=axs[3][2]).set_title("PM$_{10}$", fontsize=font_size)
# for ax in axs:
#     for a in ax:
#         a.set_xlabel("")
# plt.show()

# fig, axs = plt.subplots(ncols=3, nrows=1, figsize=(12, 4))
# sns.distplot(df["CO2"], ax=axs[0]).set_title('CO$_2$')
# sns.distplot(df["TVOC"], ax=axs[1]).set_title("TVOC")
# sns.distplot(df["AQI"], ax=axs[2]).set_title("AQI")
# for ax in axs:
#     ax.set_xlabel("")
# plt.show()

# fig, axs = plt.subplots(ncols=3, nrows=1, figsize=(12, 4))
# sns.distplot(df["SO2"], ax=axs[0]).set_title('SO$_2$')
# sns.distplot(df["NH3"], ax=axs[1]).set_title("NH$_3$")
# sns.distplot(df["O3"], ax=axs[2]).set_title("O$_3$")
# for ax in axs:
#     ax.set_xlabel("")
# plt.show()

# fig, axs = plt.subplots(ncols=3, nrows=1, figsize=(12, 4))
# sns.distplot(df["PM25"], ax=axs[0]).set_title('PM$_{2.5}$')
# sns.distplot(df["PM10"], ax=axs[1]).set_title("PM$_{10}$")
# sns.distplot(df["PM01"], ax=axs[2]).set_title("PM$_{0.1}$")
# for ax in axs:
#     ax.set_xlabel("")
# plt.show()

fig, axs = plt.subplots(ncols=3, nrows=2, figsize=(12, 8))
font_size = 15
sns.distplot(df["TEMP"], ax=axs[0, 0]).set_title('Nhiệt độ', fontsize=font_size)
sns.distplot(df["HUM"], ax=axs[0, 1]).set_title("Độ ẩm", fontsize=font_size)
sns.distplot(df["LIGHT"], ax=axs[0, 2]).set_title("Ánh sáng", fontsize=font_size)
sns.distplot(df["CO2"], ax=axs[1, 0]).set_title('CO$_2$', fontsize=font_size)
sns.distplot(df["PM25"], ax=axs[1, 1]).set_title("PM$_{2.5}$", fontsize=font_size)
sns.distplot(df["PM10"], ax=axs[1, 2]).set_title("PM$_{10}$", fontsize=font_size)
for i in range(2):
    for j in range(3):
        axs[i, j].set_xlabel("")
plt.show()

# fig, axs = plt.subplots(figsize=(5, 5))
# plt.scatter(df["PM10"], df["PM25"], s=2)
# plt.xlabel('PM$_{10}$')
# plt.ylabel('PM$_{2.5}$')
# plt.title('Tương quan giữa PM$_{10}$ và PM$_{2.5}$')
# plt.show()

# df.plot(linewidth=0.5, figsize=(20, 5))
# plt.show()