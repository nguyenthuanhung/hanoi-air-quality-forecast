import numpy as np
import pandas as pd
from sklearn import preprocessing
from sklearn.metrics import mean_absolute_error
from sklearn.metrics import mean_squared_error
from sklearn.metrics import r2_score
from sklearn.model_selection import train_test_split
from xgboost import XGBRegressor
import time

data = pd.read_csv("../data/TCM_TK_TH_season.csv")
data = data.to_numpy()

y = data[:, -1].reshape(-1, 1)
X = data[:, :-1]

X_train, X_test, y_train, y_test = train_test_split(X, y, test_size=.25, random_state=42)

scaler = preprocessing.StandardScaler()

X_train = scaler.fit_transform(X_train)
X_test = scaler.transform(X_test)
y_train = scaler.fit_transform(y_train)
y_test = scaler.transform(y_test)

xgb_regression = XGBRegressor(n_estimators=200, max_depth=8, gamma=0.7, objective="reg:squarederror")
start_time = time.time()
xgb_regression.fit(X_train, y_train)
training_time = time.time() - start_time
xgb_pred = xgb_regression.predict(X_test)
print("XGB score: {:.4f}".format(r2_score(y_test, xgb_pred)))
print("XGB MAE: {:.4f}".format(mean_absolute_error(y_test, xgb_pred)))
print("XGB RMSE: {:.4f}".format(np.sqrt(mean_squared_error(y_test, xgb_pred))))
print("Time: {}".format(training_time))
