import matplotlib.pyplot as plt
import pandas as pd

if __name__ == "__main__":
    file = ['THINKLABS_DATA.csv', 'TRINH_KHA_DATA.csv', 'VNPT_TH_DATA.csv']
    # data = pd.read_csv(file[2])
    data = pd.read_csv("../data/TCM_TK_TH_season.csv")
    # data = pd.read_csv("TCM_TK_TH_season_2.csv")
    # data = pd.read_csv("TCM_TK_TH_season_compare.csv")
    # data = pd.read_csv("HANOI_DATA.csv")
    data = data.to_numpy()

    # y = data[:, -1].reshape((-1, 1))
    # X = data[:, :-1]
    #
    # scaler = preprocessing.StandardScaler()
    # scaler_X = scaler.fit(X)
    # scaler_y = scaler.fit(y)
    #
    # X_train, X_test, y_train, y_test = train_test_split(X, y, test_size=.25, random_state=42)
    #
    # X_train = scaler_X.transform(X_train)
    # X_test = scaler_X.transform(X_test)
    # y_train = scaler_y.transform(y_train)
    # y_test = scaler_y.transform(y_test)

    # print("Method 2")

    # mlp = MLPRegression(hidden_layer_sizes=(256,), max_iter=1000, learning_rate=0.001, tol=1e-6, batch_size=192)
    # mlp.fit(X_train, y_train)
    #
    # nnr_pred = mlp.predict(X_test)
    # score = r2_score(y_test, nnr_pred)
    # mae = mean_absolute_error(y_test, nnr_pred)
    # rmse = np.sqrt(mean_squared_error(y_test, nnr_pred))
    #
    # print("R-squared: {:.4f}".format(score))
    # print("MAE: {:.4f}".format(mae))
    # print("RMSE: {:.4f}".format(rmse))
    #
    #
    # y_test_2 = scaler.inverse_transform(y_test)
    # nnr_pred_2 = scaler.inverse_transform(nnr_pred)

    # print("Method 1")

    # data = pd.read_csv("TCM_TK_TH_season_2.csv")
    # data = data.to_numpy()

    # y = data[:, -1].reshape((-1, 1))
    # X = data[:, :-1]
    #
    # scaler = preprocessing.StandardScaler()
    # scaler_X = scaler.fit(X)
    # scaler_y = scaler.fit(y)
    #
    # X_train, X_test, y_train, y_test = train_test_split(X, y, test_size=.25, random_state=42)
    #
    # X_train = scaler_X.transform(X_train)
    # X_test = scaler_X.transform(X_test)
    # y_train = scaler_y.transform(y_train)
    # y_test = scaler_y.transform(y_test)

    # mlp = MLPRegression(hidden_layer_sizes=(128, 96,), max_iter=1000, learning_rate=0.001, tol=1e-6, batch_size=192)
    # mlp = MLPRegression(hidden_layer_sizes=(128, 96,), max_iter=1000, learning_rate=0.0005, tol=1e-6, batch_size=192)
    # mlp = MLPRegression(hidden_layer_sizes=(256,), max_iter=1000, learning_rate=0.001, tol=1e-6, batch_size=192)
    # mlp = MLPRegression(hidden_layer_sizes=(256,), max_iter=1000, learning_rate=0.0005, tol=1e-6, batch_size=192)
    # mlp.fit(X_train, y_train)

    # nnr_pred = mlp.predict(X_test)
    # score = r2_score(y_test, nnr_pred)
    # mae = mean_absolute_error(y_test, nnr_pred)
    # rmse = np.sqrt(mean_squared_error(y_test, nnr_pred))

    # print("R-squared: {:.4f}".format(score))
    # print("MAE: {:.4f}".format(mae))
    # print("RMSE: {:.4f}".format(rmse))

    # y_test_1 = scaler.inverse_transform(y_test)
    # nnr_pred_1 = scaler.inverse_transform(nnr_pred)

    fig, axs = plt.subplots(1, 2, figsize=(10, 5))
    # fig.suptitle("$R^2$-score = {:.4f}\tMAE = {:.4f}\tRMSE = {:.4f}".format(score, mae, rmse), fontsize=14)
    # plt.tight_layout()

    axs[0].plot([])
    axs[0].set_xlabel('Vòng lặp')
    axs[0].set_ylabel('Hàm mất mát')
    axs[0].set_title('Phương pháp 1')

    axs[1].scatter([], [], s=4, zorder=1, alpha=0.75)
    # axs[1].plot(np.arange(np.amax([])), 'r--', zorder=2, label="Giá trị dự đoán lý tưởng", linewidth=0.75)
    axs[1].set_xlabel('Giá trị thực tế')
    axs[1].set_ylabel('Giá trị dự đoán')
    axs[1].legend(loc='upper left')
    axs[1].set_title('Phương pháp 2')
    
    # axs[1].scatter(y_test_2, nnr_pred_2, s=4, zorder=1, alpha=0.75)
    # axs[1].plot(np.arange(np.amax([y_test_2, nnr_pred_2])), 'r--', zorder=2, label="Perfect prediction", linewidth=0.75)
    # axs[1].set_xlabel('Measured')
    # axs[1].set_ylabel('Predicted')
    # axs[1].legend(loc='upper left')
    # axs[1].set_title('Method 2')
    
    plt.show()