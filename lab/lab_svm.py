import numpy as np
import pandas as pd
from sklearn import preprocessing
from sklearn.metrics import mean_absolute_error
from sklearn.metrics import mean_squared_error
from sklearn.metrics import r2_score
from sklearn.model_selection import train_test_split
from sklearn.svm import SVR

data = pd.read_csv("../data/TCM_TK_TH_season.csv")
data = data.to_numpy()

y = data[:, -1].reshape(-1, 1)
X = data[:, :-1]

X_train, X_test, y_train, y_test = train_test_split(X, y, test_size=.25, random_state=60)

scaler = preprocessing.StandardScaler()

X_train = scaler.fit_transform(X_train)
X_test = scaler.transform(X_test)
y_train = scaler.fit_transform(y_train)
y_test = scaler.transform(y_test)

svr = SVR(gamma='auto', C=100, epsilon=0.0001)
svr.fit(X_train, y_train)
svr_pred = svr.predict(X_test)
print("SVR regression score: {:.4f}".format(r2_score(y_test, svr_pred)))
print("SVR regression MAE: {:.4f}".format(mean_absolute_error(y_test, svr_pred)))
print("SVR regression RMSE: {:.4f}".format(np.sqrt(mean_squared_error(y_test, svr_pred))))
