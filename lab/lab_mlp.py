import time

import numpy as np
import pandas as pd
from sklearn import preprocessing
from sklearn.metrics import mean_absolute_error
from sklearn.metrics import mean_squared_error
from sklearn.metrics import r2_score
from sklearn.model_selection import train_test_split
from sklearn.neural_network import MLPRegressor

data = pd.read_csv("../data/TCM_TK_TH_season.csv")
data = data.to_numpy()

y = data[:, -1].reshape(-1, 1)
X = data[:, :-1]

X_train, X_test, y_train, y_test = train_test_split(X, y, test_size=.25, random_state=42)

scaler = preprocessing.StandardScaler()

X_train = scaler.fit_transform(X_train)
X_test = scaler.transform(X_test)
y_train = scaler.fit_transform(y_train)
y_test = scaler.transform(y_test)

mlp = MLPRegressor(hidden_layer_sizes=(192, 128, 96), activation='relu', learning_rate_init=0.001, tol=1e-6,
                   batch_size=256)
start_time = time.time()
mlp.fit(X_train, y_train)
end_time = time.time()
mlp_pred = mlp.predict(X_test)
print("MLP regression score: {:.4f}".format(r2_score(y_test, mlp_pred)))
print("MLP regression MAE: {:.4f}".format(mean_absolute_error(y_test, mlp_pred)))
print("MLP regression RMSE: {:.4f}".format(np.sqrt(mean_squared_error(y_test, mlp_pred))))
print("Time: {}".format(end_time - start_time))
