
import datetime
import pandas as pd
import seaborn as sns
import numpy as np
import matplotlib.pyplot as plt

df = pd.read_csv("../data/iot-aqm.continuousdata.csv")
df = df.drop(["_id", "data.0.status", "data.0.unit", "data.1.index", "data.1.status", "data.1.unit",
              "data.1.value", "data.2.index", "data.2.status", "data.2.unit", "data.2.value",
              "data.3.status", "data.3.unit", "data.4.status", "data.4.unit", "data.5.status",
              "data.5.unit", "data.6.index", "data.6.status", "data.6.unit", "data.6.value",
              "node", "oganizationCode"], axis=1)

even_df = df.iloc[::2]
even_df = even_df.reset_index()
even_df = even_df.drop(["index", "data.0.index", "data.0.value", "data.3.index", "data.3.value"], axis=1)

odd_df = df.iloc[1::2]
odd_df = odd_df.reset_index()
odd_df = odd_df.drop(["index", "time"], axis=1)

df = pd.concat([odd_df, even_df], axis=1, sort=False)
columns = ["col1", "CO2", "col2", "TEMP", "col3", "HUM", "col4", "LIGHT", "col5", "PM25", "col6", "PM10", "time"]
df.columns = columns

df = df.drop(["col1", "col2", "col3", "col4", "col5", "col6"], axis=1)
df = df.dropna()
df = df.drop(df[df.TEMP > 50].index)
df = df.drop(df[df.HUM > 100].index)
df = df.drop(df[df.CO2 > 5000].index)
df = df.drop(df[df.CO2 < 400].index)
df = df.drop(df[df.LIGHT > 800].index)
df = df.drop(df[df.PM25 > 200].index)
df = df.drop(df[df.PM10 > 200].index)

fig, axs = plt.subplots(ncols=3, nrows=2, figsize=(12, 8))
sns.distplot(df["PM25"], ax=axs[0, 0]).set_title("PM25")
sns.distplot(df["PM10"], ax=axs[0, 1]).set_title("PM10")
sns.distplot(df["CO2"], ax=axs[0, 2]).set_title("CO2")
sns.distplot(df["TEMP"], ax=axs[1, 0]).set_title("TEMP")
sns.distplot(df["HUM"], ax=axs[1, 1]).set_title("HUM")
sns.distplot(df["LIGHT"], ax=axs[1, 2]).set_title("LIGHT")
plt.show()