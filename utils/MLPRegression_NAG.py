import numpy as np
from sklearn.utils import shuffle


class MLPRegression:

    def __init__(self, hidden_layer_sizes=(200,), batch_size=150, learning_rate=2e-3, max_iter=1500,
                 n_iter_no_change=10, tol=1e-6, random_seed=42, gamma=0.9):
        self.hidden_layer_sizes = list(hidden_layer_sizes)
        self.batch_size = batch_size
        self.learning_rate = learning_rate
        self.max_iter = max_iter
        self.n_iter_no_change = n_iter_no_change
        self.tol = tol
        self.random_state = np.random.RandomState(random_seed)
        self.gamma = gamma
        self.weight = []
        self.bias = []
        self.alpha = 0.0001
        self.n_samples = None
        self.n_outputs = None

    @staticmethod
    def __squared_loss(y_true, y_pred):
        return np.square(y_true - y_pred).mean() / 2

    def __initialize(self, X, layer_units):
        self.n_layers = len(layer_units)

        for i in range(self.n_layers - 1):
            bound = np.sqrt(6. / (layer_units[i] + layer_units[i + 1]))
            w_init = self.random_state.uniform(-bound, bound, (layer_units[i], layer_units[i + 1]))
            b_init = self.random_state.uniform(-bound, bound, (layer_units[i + 1], 1))
            self.weight.append(w_init)
            self.bias.append(b_init)

        self.weight.insert(0, [])
        self.bias.insert(0, [])

        activations = [X]
        activations.extend(np.empty((n_units, self.batch_size)) for n_units in layer_units[1:])

        errors = [] + [np.empty_like(activation) for activation in activations]

        w_grads = [np.empty((n_units_in, n_unit_out)) for n_units_in, n_unit_out in
                   zip(layer_units[:-1], layer_units[1:])]
        w_grads.insert(0, np.empty((1, 1)))

        b_grads = [np.empty(n_unit_out) for n_unit_out in layer_units[1:]]
        b_grads.insert(0, np.empty((1, 1)))

        return activations, errors, w_grads, b_grads

    def __generate_batch(self):
        start = 0
        for i in range(int(self.n_samples // self.batch_size)):
            end = start + self.batch_size
            if end > self.n_samples:
                continue
            yield slice(start, end)
            start = end
        if start < self.n_samples:
            yield slice(start, self.n_samples)

    def __feed_forward(self, activations):
        i = 0
        for i in range(1, self.n_layers - 1):
            activations[i] = np.dot(self.weight[i].T, activations[i - 1]) + self.bias[i]
            # ReLU
            activations[i] = np.maximum(activations[i], 0)
            # Sigmoid
            # activations[i] = 1 / (1 + np.exp(-activations[i]))

        i += 1
        activations[i] = np.dot(self.weight[i].T, activations[i - 1])

    def __back_propagation(self, X, y, activations, errors, w_grads, b_grads, w_momentum, b_momentum):
        size = X.shape[1]

        self.__feed_forward(activations)

        batch_loss = self.__squared_loss(y, activations[-1])

        errors[-1] = (activations[-1] - y) / size

        for i in range(self.n_layers - 1, 0, -1):
            w_grads[i] = np.dot(activations[i - 1], errors[i].T)
            b_grads[i] = np.sum(errors[i], axis=1, keepdims=True)
            errors[i - 1] = np.dot(self.weight[i], errors[i])
            # ReLU
            errors[i - 1][activations[i - 1] == 0] = 0
            # Sigmoid
            # errors[i - 1] *= activations[i - 1]
            # errors[i - 1] *= (1 - activations[i - 1])

            w_momentum[i] = self.gamma * w_momentum[i] + self.learning_rate * w_grads[i]
            self.weight[i] -= w_momentum[i]

            b_momentum[i] = self.gamma * b_momentum[i] + self.learning_rate * b_grads[i]
            self.bias[i] -= b_momentum[i]

        return batch_loss, w_grads, b_grads

    def __check_convergence(self):
        if len(self.gd_loss) <= self.n_iter_no_change:
            return False

        n_iter = 0
        last_gd_loss = self.gd_loss[-(self.n_iter_no_change + 1):]
        for i in range(1, len(last_gd_loss)):
            if np.abs(last_gd_loss[i] - last_gd_loss[i - 1]) < self.tol:
                n_iter += 1

        if n_iter == self.n_iter_no_change:
            return True
        else:
            return False

    def __sgd(self, X, y, activations, errors, w_grads, b_grads):
        self.gd_loss = []
        w_momentum = np.zeros_like(w_grads)
        b_momentum = np.zeros_like(b_grads)
        for i in range(self.max_iter):
            X, y = shuffle(X, y, random_state=self.random_state)
            iter_loss = 0.0
            for batch in self.__generate_batch():
                activations[0] = X[batch].T
                batch_loss, w_grads, b_grads = self.__back_propagation(X[batch].T, y[batch].T, activations, errors,
                                                                       w_grads, b_grads, w_momentum, b_momentum)
                iter_loss += batch_loss

            # values = 0
            # for j in range(1, self.n_layers):
            #     values += np.sum(self.weight[j] ** 2)
            # iter_loss += self.alpha / 2 * values

            self.gd_loss.append(iter_loss / self.n_samples)

            # if i % 100 == 0:
            #     print(f"Loss = {self.gd_loss[-1]} after {i} iterations")

            if self.__check_convergence():
                break

    def fit(self, X, y):
        self.n_samples, n_features = X.shape

        if y.ndim == 1:
            y = y.reshape((-1, 1))

        self.n_outputs = y.shape[1]

        layer_units = ([n_features] + self.hidden_layer_sizes + [self.n_outputs])

        activations, errors, w_grads, b_grads = self.__initialize(X, layer_units)

        self.__sgd(X, y, activations, errors, w_grads, b_grads)

    def predict(self, X):
        n_samples, n_features = X.shape
        layer_units = [n_features] + self.hidden_layer_sizes + [self.n_outputs]

        activations = [X.T]
        activations.extend(np.empty((n_units, n_samples)) for n_units in layer_units[1:])

        self.__feed_forward(activations)

        return activations[-1].T

    def get_loss(self):
        return self.gd_loss
