import numpy as np
import pandas as pd
from sklearn import preprocessing
from sklearn.ensemble import RandomForestRegressor
from sklearn.metrics import r2_score
from sklearn.model_selection import train_test_split
import matplotlib.pyplot as plt


class DecisionTreeRegression:

    def __init__(self, X, y, n_features, f_index, index, depth=10, min_leaf=5):
        self.X = X
        self.y = y
        self.n_features = n_features
        self.f_index = f_index
        self.index = index
        self.depth = depth
        self.min_leaf = min_leaf
        self.n_samples = len(index)
        self.val = np.mean(y[index])
        self.score = np.inf
        self.__find_var_split()

    @staticmethod
    def __std_deviation(count, _sum, sum_square):
        return np.sqrt((sum_square / count) - (_sum / count) ** 2)

    def __find_var_split(self):
        for i in self.f_index:
            self.__find_better_split(i)

        if self.__is_leaf():
            return

        X_split = self.__split_col()
        left = np.nonzero(X_split <= self.split)[0]
        right = np.nonzero(X_split > self.split)[0]
        lf_index = np.random.permutation(self.X.shape[1])[:self.n_features]
        rf_index = np.random.permutation(self.X.shape[1])[:self.n_features]
        self.left_tree = DecisionTreeRegression(self.X, self.y, self.n_features, lf_index, self.index[left],
                                                depth=self.depth - 1,
                                                min_leaf=self.min_leaf)
        self.right_tree = DecisionTreeRegression(self.X, self.y, self.n_features, rf_index, self.index[right],
                                                 depth=self.depth - 1,
                                                 min_leaf=self.min_leaf)

    def __find_better_split(self, var_index):
        X_split = self.X[self.index, var_index]
        y_split = self.y[self.index]

        sorted_index = np.argsort(X_split)
        sorted_X = X_split[sorted_index]
        sorted_y = y_split[sorted_index]

        right_count, right_sum, right_sum_square = self.n_samples, sorted_y.sum(), (sorted_y ** 2).sum()
        left_count, left_sum, left_sum_square = 0, 0., 0.

        for i in range(0, self.n_samples - self.min_leaf - 1):
            X_i, y_i = sorted_X[i], sorted_y[i]
            left_count += 1
            right_count -= 1
            left_sum += y_i
            right_sum -= y_i
            y_i_2 = y_i ** 2
            left_sum_square += y_i_2
            right_sum_square -= y_i_2
            if i < self.min_leaf or X_i == sorted_X[i + 1]:
                continue

            left_std = self.__std_deviation(left_count, left_sum, left_sum_square)
            right_std = self.__std_deviation(right_count, right_sum, right_sum_square)
            split_score = left_std * left_count + right_std * right_count

            if split_score < self.score:
                self.var_index = var_index
                self.score = split_score
                self.split = X_i

    def __split_col(self):
        return self.X[self.index, self.var_index]

    def __is_leaf(self):
        return self.score == np.inf or self.depth <= 0

    def __predict_row(self, X):
        if self.__is_leaf():
            return self.val

        if X[self.var_index] <= self.split:
            return self.left_tree.__predict_row(X)
        else:
            return self.right_tree.__predict_row(X)

    def predict(self, X):
        return np.array([self.__predict_row(X_i) for X_i in X])


class RandomForestRegression:

    def __init__(self, n_trees=3, n_features=None, n_samples='auto', depth=10, min_leaf=5):
        self.n_trees = n_trees
        self.n_samples = n_samples
        self.depth = depth
        self.min_leaf = min_leaf
        self.n_features = n_features
        self.trees = None
        self.total_samples = None
        self.total_features = None

    def __create_tree(self, X, y):
        # sample index
        s_index = np.random.permutation(y.shape[0])[:self.n_samples]
        # feature index
        f_index = np.random.permutation(self.total_features)[:self.n_features]
        return DecisionTreeRegression(X[s_index], y[s_index], self.n_features, f_index,
                                      index=np.array(range(self.n_samples)), depth=self.depth, min_leaf=self.min_leaf)

    def fit(self, X, y):
        self.total_samples, self.total_features = X.shape

        if y.ndim == 1:
            y = y.reshape((-1, 1))

        self.n_features = int(np.sqrt(self.total_features))

        if self.n_samples == 'auto':
            self.n_samples = int(0.8 * self.total_samples)

        self.trees = [self.__create_tree(X, y) for _ in range(self.n_trees)]

    def predict(self, X):
        return np.mean([tree.predict(X) for tree in self.trees], axis=0)


if __name__ == '__main__':
    # file = ['THINKLABS_DATA.csv', 'TRINH_KHA_DATA.csv', 'HANOI_DATA.csv']
    # data = pd.read_csv(file[2])
    data = pd.read_csv("../data/HANOI_DATA.csv")
    data = data.to_numpy()

    y = data[:, -1].reshape((-1, 1))
    X = data[:, :-1]

    scaler = preprocessing.StandardScaler()
    scaler_X = scaler.fit(X)
    scaler_y = scaler.fit(y)

    X_train, X_test, y_train, y_test = train_test_split(X, y, test_size=.25, random_state=42)

    X_train = scaler_X.transform(X_train)
    X_test = scaler_X.transform(X_test)
    y_train = scaler_y.transform(y_train)
    y_test = scaler_y.transform(y_test)

    rf = RandomForestRegression(n_trees=30)
    rf.fit(X_train, y_train)

    rf_pred = rf.predict(X_test)
    print("R-squared: {:.4f}".format(r2_score(y_test, rf_pred)))

    plt.scatter(scaler.inverse_transform(y_test), scaler.inverse_transform(rf_pred))
    plt.xlabel('Measured')
    plt.ylabel('Predicted')
    plt.title('Decision Forest Predicted vs Actual')
    plt.show()

    rf_regression = RandomForestRegressor(n_estimators=100, random_state=42)
    rf_regression.fit(X_train, y_train)

    rf_score = rf_regression.score(X_test, y_test)
    print("Random forest regression score: {:.4f}".format(rf_score))

    rf_pred = rf_regression.predict(X_test)

    plt.scatter(scaler.inverse_transform(y_test), scaler.inverse_transform(rf_pred))
    plt.xlabel('Measured')
    plt.ylabel('Predicted')
    plt.title('Decision Forest Predicted vs Actual_sklearn')
    plt.show()
