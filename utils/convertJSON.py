import json
from datetime import datetime as dt
import csv
import math


def convert_to_date_time(date):
    time = date.split(":")
    time[2] = str(int(math.floor(float(time[2][:-1]))))
    date = time[0] + ":" + time[1] + ":" + time[2]

    return dt.strptime(date, '%Y-%m-%dT%H:%M:%S')


stations = ["THINKLABS", "TRINH_KHA", "VNPT_TH"]
header = ['time', 'SO2', 'CO', 'NH3', 'O3', 'PM25', 'PM10',
          'AQI', 'CO2', 'PM01', 'TVOC', 'TEMP', 'HUM', 'LIGHT', 'node']

for station in stations:
    data = []
    with open(station + ".json", "r", encoding="utf-8") as fp:
        data = json.loads(fp.read())
        fp.close()

    dataLength = len(data)
    print("{} {}".format(station, dataLength))

    csvFile = open(station + '.csv', 'w', newline='')
    csvFile.close()
    csvFile = open(station + '.csv', 'a', newline='')
    writer = csv.writer(csvFile)
    writer.writerow(header)

    i = 0
    while i < dataLength-1:
        element = data[i]
        nextElement = data[i+1]

        time = convert_to_date_time(element['time'])
        nextTime = convert_to_date_time(nextElement['time'])

        row = [time]
        elementData = element['data']
        nextElementData = nextElement['data']

        if elementData[0]['index'] == "SO2" and nextElementData[0]['index'] == "CO2":
            for e in elementData:
                row.append(e['value'])
            for e in nextElementData:
                row.append(e['value'])
            i = i + 1
        elif elementData[0]['index'] == "SO2":
            for e in elementData:
                row.append(e['value'])
            for j in range(6):
                row.append("")
        else:
            for j in range(7):
                row.append("")
            for e in elementData:
                row.append(e['value'])

        i = i + 1
        row.append(element['node'])
        writer.writerow(row)

    csvFile.close()
