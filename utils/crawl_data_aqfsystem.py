import datetime
import requests
from datetime import datetime as dt
import mysql.connector as connector
from mysql.connector import Error


class Crawler:

    def __init__(self):
        self.data = {}
        self.idx = ['TVOC', 'TEMP', 'HUM', 'PM01', 'CO2', 'NH3', 'LIGHT', 'AQI', 'PM25', 'PM10', 'CO', 'O3', 'SO2']

    @staticmethod
    def convert_to_date_time(date):
        time = date.split(":")
        date = time[0] + ":" + time[1] + ":00"
        date = dt.strptime(date, '%Y-%m-%dT%H:%M:%S')
        return date.strftime('%Y-%m-%d %H:%M:%S')

    def crawl_data(self, index, date):
        data_url = "https://aqm-dashboard-v3.mybluemix.net/api/devices/THINKLABS_HN_1_MCU_1/history"
        params = {'type': 'oneday', 'index': index, 'startDate': date}
        get_data_request = requests.get(url=data_url, params=params)
        data = get_data_request.json()["record_history"]

        if len(data) == 0:
            data_url = "https://aqm-dashboard-v3.mybluemix.net/api/devices/THINKLABS_HN_2_MCU_2/history"
            get_data_request = requests.get(url=data_url, params=params)
            data = get_data_request.json()["record_history"]
            if len(data) == 0:
                return

        print(f"Crawling {index} on {date}")
        for d in data:
            time = self.convert_to_date_time(d['time'])
            avg = d['avg']

            try:
                self.data[time][index] = avg
            except KeyError:
                self.data[time] = {}
                self.data[time][index] = avg

    def crawl_data_previous_day(self):
        date = datetime.date.today() - datetime.timedelta(days=1)
        for index in self.idx:
            self.crawl_data(index, date)

        print(self.data)

        self.write_to_database()

    def crawl_data_from(self, start_date):
        today = datetime.date.today()
        delta = today - start_date
        dates = [(today - datetime.timedelta(days=i)).strftime("%Y-%m-%d") for i in range(1, delta.days + 1)]
        dates = reversed(dates)
        for date in dates:
            for index in self.idx:
                self.crawl_data(index, date)

            if len(self.data) > 0:
                self.write_to_database()
            else:
                time = date.split("-")
                hours = [(datetime.datetime(int(time[0]), int(time[1]), int(time[2])) + datetime.timedelta(hours=i)).strftime("%Y-%m-%d %H:%M:%S") for i in range(0, 24)]
                try:
                    connection = connector.connect(host='localhost', port=3306, database='aqfsystem',
                                                   user='root', password='root')
                    cursor = connection.cursor(prepared=True)
                    query = "INSERT INTO data (time, station_id) VALUES (%s, 1)"
                    for hour in hours:
                        cursor.execute(query, tuple([hour]))
                    connection.commit()
                    cursor.close()
                    connection.close()
                except Error as e:
                    return
            self.data = {}
            print("----------------------------------------------------")

    def write_to_database(self):
        try:
            connection = connector.connect(host='localhost', port=3306, database='aqfsystem', user='root', password='root')
            cursor = connection.cursor(prepared=True)
            query = "INSERT INTO data ({}, time, station_id) VALUES ({}, %s, 1)"
            separator = ","
            for key, value in self.data.items():
                insert_data = []
                attrs = []
                params = []
                for k, v in value.items():
                    attrs.append(k.lower())
                    params.append("%s")
                    insert_data.append(v)
                insert_data.append(key)
                query = query.format(separator.join(attrs), separator.join(params))
                cursor.execute(query, tuple(insert_data))
                connection.commit()
            cursor.close()
            connection.close()
        except Error as e:
            print(f"{e}")


crawler = Crawler()
# crawler.crawl_data_previous_day()
crawler.crawl_data_from(datetime.date(2019, 11, 3))
